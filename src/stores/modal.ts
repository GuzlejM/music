import { defineStore } from 'pinia'

interface ModalState {
  isOpen: boolean
}

export const useModalStore = defineStore('modal', {
  state: (): ModalState => ({
    isOpen: false
  }),
  actions: {
    toggle() {
      this.isOpen = !this.isOpen
    },
    open() {
      this.isOpen = true
    },
    close() {
      this.isOpen = false
    }
  },
  getters: {
    hiddenClass(state): string {
      // When using the function form, you can directly use the state parameter
      return !state.isOpen ? 'hidden' : ''
    }
  }
})
