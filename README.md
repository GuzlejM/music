# Music App

Welcome to the Music App, a modern web application for experiencing and managing music. Built with Vue 3, leveraging the composition API for state management via Pinia, and utilizing Vue Router for navigation, this project is set up with Vite as the build tool for an efficient development experience.

## Prerequisites

Before running this application, ensure you have the following installed on your system:

- Node.js (Preferably the latest stable version)
- npm (Node Package Manager)

## Installation

To get started with the Music App, clone the repository to your local machine or download the source code. After that, navigate to the project directory and run the following command to install the dependencies:

### `npm install`

This command will install all the necessary dependencies listed in the `package.json` file.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in development mode using Vite.
Open [http://localhost:4173](http://localhost:4173) to view it in the browser.
The page will automatically reload if you make changes to the code.

### `npm run build`

Builds the app for production to the `dist` folder using Vite.
It correctly bundles Vue in production mode and optimizes the build for the best performance.

### `npm run preview`

Locally preview the production build.

### `npm run test:unit`

Executes unit tests with Vitest.

### `npm run test:e2e`

Runs end-to-end tests with Cypress after starting the preview server.

### `npm run test:e2e:dev`

Starts the development server and opens Cypress for end-to-end testing in interactive mode.

### `npm run type-check`

Performs TypeScript type-checking.

### `npm run lint`

Lints and fixes files based on ESLint rules.

### `npm run format`

Formats the code using Prettier.

## Folder Structure

The main directories and files in this project:

- `src/`: Contains the source code of the Vue application.
- `tests/`: Contains all the unit and end-to-end test files.
- `index.html`: Entry point for the application.
- `vite.config.js`: Configuration file for Vite.
- `tsconfig.json`: Configuration file for TypeScript.

## Dependency Documentation

For more details about the packages used in this application, refer to the following documentation:

- Vue 3: [https://v3.vuejs.org/](https://v3.vuejs.org/)
- Pinia: [https://pinia.vuejs.org/](https://pinia.vuejs.org/)
- Vue Router: [https://router.vuejs.org/](https://router.vuejs.org/)
- Vite: [https://vitejs.dev/](https://vitejs.dev/)
- Cypress: [https://www.cypress.io/](https://www.cypress.io/)
- Vitest: [https://vitest.dev/](https://vitest.dev/)
- TypeScript: [https://www.typescriptlang.org/](https://www.typescriptlang.org/)

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks again!

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Your Name - [your-email@example.com](mailto:your-email@example.com)

Project Link: [https://github.com/your-username/music](https://github.com/your-username/music)

